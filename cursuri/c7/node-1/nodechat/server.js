//explained example in Node Up and Running
const net = require('net')

let server = net.createServer(),
	clients = []

server.on('connection', (client) => {
	client.write('welcome to the wonderful chat server!!! \n')
	clients.push(client)
	client.on('data',(data) => {
		for (let i = clients.length - 1; i >= 0; i--) {
			if (clients[i] != client)
				clients[i].write(data)
		}
	})

	client.on('end',() => {
		clients.splice(clients.indexOf(client),1)
	})
})

server.listen(3333)