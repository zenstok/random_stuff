const express = require('express')
const session = require('express-session')

const app = express()
app.set('view engine', 'pug')
app.use(session({secret : 'really secret secret'}))
app.locals.products = [{id:1, name : 'aaa', price : 10, image : 'somelink.jpg'},{id:2, name : 'bbb', price : 10, image : 'somelink.jpg'}]


app.get('/', (req, res) => {
	let session = req.session
	session.cart = []
	res.render('index', {products : app.locals.products, cart : session.cart})
})

app.get('/add/:id', (req, res) => {
	let session = req.session
	let id = req.params.id
	let productList = app.locals.products.filter((e) => e.id == id)
	if (productList.length > 0){
		let p = productList[0]
		let sessionProduct = session.cart.filter((e) => e.id == id)
		if (sessionProduct.length > 0){
			sessionProduct[0].count++
		}
		else{
			p.count = 1
			session.cart.push(p)			
		}
	}
	res.render('index', {products : app.locals.products, cart : session.cart})
})

app.listen(8080)