const express = require('express')
const bodyParser = require('body-parser')

const app = express()
app.use(bodyParser.json())
app.use(express.static('app'))
app.use((req, res, next) => {
	console.warn(req.method + ' ' +req.url)
	next()
})

app.locals.widgets = [{id : 1, name : 'testWidget', type : 'special'}]

app.get('/widgets', (req, res) => {
	res.status(200).json(app.locals.widgets)
})

app.post('/widgets', (req, res) => {
	app.locals.widgets.push(req.body)
	res.status(201).json({message : 'created'})
})

app.get('/widgets/:id', (req, res) => {
	let widget = app.locals.widgets.find((e) => e.id == req.params.id)
	if (!widget){
		res.status(404).json({message : 'not found'})
	}
	else{
		res.status(200).json(widget)
	}
})

app.put('/widgets/:id', (req, res) => {
	let widgetIndex = app.locals.widgets.findIndex((e) => e.id == req.params.id)
	app.locals.widgets[widgetIndex].name = req.body.name
	app.locals.widgets[widgetIndex].type = req.body.type
	res.status(202).json({message : 'accepted'})
})

app.delete('/widgets/:id', (req, res) => {
	let widgetIndex = app.locals.widgets.findIndex((e) => e.id == req.params.id)
	app.locals.widgets.splice(widgetIndex, 1)
	res.status(202).json({message : 'accepted'})	
})


app.listen(8080)

