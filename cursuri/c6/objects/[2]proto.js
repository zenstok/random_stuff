function SomeParentType(a){
	this.a = a
	this.doParent = function(x){
		console.log('doing parent stuff with x = ' + x + ' and a = ' + this.a)
	}
}

let o0 = new SomeParentType(3)
o0.doParent(4)

function SomeChildType(b){
	this.b = b
	this.doChild = function(x){
		console.log('doing child stuff with x = ' + x + ' and a = ' + this.b)
	}
}

SomeChildType.prototype = new SomeParentType(5)


let o1 = new SomeChildType(6)
o1.doChild(0)
o1.doParent(1)


SomeChildType.prototype.doNewStuff = function(x){
	console.log('doing some new stuff with x = ' + x)
}

o1.doNewStuff(4)

let f0 = o1.doParent
f0(5)
f0.apply(o1, [5])

