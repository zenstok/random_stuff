'use strict'
let o0 = {
	a : 1, 
	b : 'somestring',
	printMe : function(){
		console.log(this.a + this.b)
	}
}
console.log(o0)
console.log(o0.__proto__)
o0.printMe()

function SomeType(a, b){
	this.a = a
	this.b = b
	this.printMe = function(){
		console.log(this.a + this.b)
	}
}

let o1 = new SomeType(1, 'a')
let o2 = new SomeType(2, 'b')
o1.printMe()
o2.printMe()

class SomeOtherType{
	constructor(a,b){
		this.a = a
		this.b = b
	}
	printMe(){
		console.log(this.a + this.b)	
	}
}

let o3 = new SomeOtherType(3,'c')
o3.printMe()