class SomeParentType{
	constructor(a){
		this.a = a
	}
	doParent(x){
		console.log('doing parent stuff with x = ' + x + ' and a = ' + this.a)
	}
}

let o0 = new SomeParentType(3)
o0.doParent(4)

class SomeChildType extends SomeParentType{
	constructor(a, b){
		super(a)
		this.b = b	
	}
	doChild(x){
		console.log('doing child stuff with x = ' + x + ' and a = ' + this.b)
	}
}


let o1 = new SomeChildType(5,6)
o1.doChild(0)
o1.doParent(1)

SomeChildType.prototype.doNewStuff = function(x){
	console.log('doing some new stuff with x = ' + x)
}

o1.doNewStuff(4)
