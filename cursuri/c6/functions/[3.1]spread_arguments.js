function f(x, y,...args){
	return args.reduce((a,e) => a + e, x + y)
}

console.log(f(1,2,3,4,5,6))