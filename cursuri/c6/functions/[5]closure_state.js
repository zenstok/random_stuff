let g = function(name){
	let n = 0
	return function(){
		n = n + 1
		console.log('for ' + name + ' current value of n ' + n)
	}
}

let f0 = g('first')
let f1 = g('second')

f0()
f0()
f0()
f1()