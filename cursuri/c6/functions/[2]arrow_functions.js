function f0(x){
	return x * 2
}

console.log('ordinary function')
console.log(f0(4))

let f1 = function(x){
	return x * 2	
}

console.log('function in a variable')
console.log(f1(5))

let f2 = (x) => x * 2
console.log('arrow function')
console.log(f2(6))

let f3 = (x) => {
	return x * 2
}
console.log('arrow function with return')
console.log(f3(7))

let functionGenerator = function(n){
	return (x) => x * n
} 
let f4 = functionGenerator(2)
console.log('generated function')
console.log(f4(8))


