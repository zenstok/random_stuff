function f(...args){
	return args.reduce((a,e) => a + e, 0)
}

function memoize(f){
	let cache = []
	return function(...args){
		console.log('called ' + arguments.length)
		for (let i = 0; i < cache.length; i++) {
			if (cache[i].args.length == args.length){
				let found = true
				for (let j = 0; j < cache[i].args.length; j++){
					if (cache[i].args[j] != args[j]){
						found = false
					}
				}
				if (found){
					console.log('from cache')
					console.log(cache[i])
					return cache[i].result
				}
			}
		}
		console.log('calling ' + args)
		let result = f(...args)
		cache.push({args : args, result : result})
		console.log(cache)
		return result
	}
}

let f1 = memoize(f)
console.log(f1(1,2,3))
console.log(f1(1,2,3,4))
console.log(f1(1,2,3))
