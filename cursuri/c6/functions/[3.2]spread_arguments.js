function f1(a,b,c){
	console.log('a, b and c were :' + a + b + c)
	let sum = Array.prototype.slice.call(arguments,3, arguments.length).reduce((a,e) => a + e, 0)
	console.log('the sum of the remaining arguments was : ' + sum)
}
f1(1,2,3)
f1(1,2,3,4,5,6)
function f2(a,b,c,...rest){
	console.log('a, b and c were :' + a + b + c)
	let sum = rest.reduce((a,e) => a + e, 0)
	console.log('the sum of the remaining arguments was : ' + sum)	
}
f2(1,2,3)
f2(1,2,3,4,5,6)
