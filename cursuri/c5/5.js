let d = [1,2,3,4,5]

for (let i = 0; i < d.length; i++) {
  console.log('d[' + i + '] = ' + d[i])
}

for (let e of d){
  console.log(e)
}

console.log(d.indexOf(3))
console.log(d.indexOf('q'))
