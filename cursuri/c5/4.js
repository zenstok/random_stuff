let a = [1,2,3]
console.log(a[1])
a[3] = 4
console.log(a)
a[8] = 9
console.log(a)
console.log(a.length)
a.push(10)
console.log(a)
console.log(a.pop())
console.log(a)
console.log(a.shift())
console.log(a)
let b = [1,2,3,4]
let c = b.slice(1,3)
console.log(c)
console.log(b.splice(1,2,'a','b','c','d'))
console.log(b)

