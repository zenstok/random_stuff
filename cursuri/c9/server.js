'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const customerRouter = require('./routers/customer-router')

const app = express()
app.use(bodyParser.json())
app.use(express.static('app'))

app.use('/api', customerRouter)

app.listen(8080)