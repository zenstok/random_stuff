module.exports = (sequelize, DataTypes) => {
  return sequelize.define('customer', {
  	name : {
	    type : DataTypes.STRING,
	    allowNull : false
	},
	address : {
	    type : DataTypes.STRING,
	    allowNull : false
	},	
	email : {
	    type : DataTypes.STRING,
	    allowNull : false,
	    validate : {
	        isEmail : true
	    }
	},	
	phone : {
	    type : DataTypes.STRING,
	    allowNull : false,
	    validate : {
	        is : /^0[0-9]{9}$/
	    }
	}
	})
}