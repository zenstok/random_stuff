function applyBlackFriday(products, discount){
	if(typeof discount !== 'number')
		return Promise.reject(new Error("Invalid discount"));

	if(discount<=0 || discount>10)
		return Promise.reject(new Error("Discount not applicable"));	

	let vector = products;
	for(let i;i<products.length;i++){
		if(typeof products[i]['name'] !== 'string')
			Promise.reject(new Error("Invalid array format"));
		if(typeof products[i]['price'] !== 'number')
			Promise.reject(new Error("Invalid array format"));
		vector[i]['price'] -= discount;
	}

	return vector;
}

const app = {
    applyBlackFriday: applyBlackFriday
};
module.exports = app;